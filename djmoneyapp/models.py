from djmoney.models.fields import MoneyField
from django.db import models
from djmoney.contrib.exchange.backends import FixerBackend, OpenExchangeRatesBackend


class TimeStamp(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(null=True, blank=True)
    is_active = models.BooleanField(default=True, null=True, blank=True)

    class Meta:
        abstract = True


class BankAccount(TimeStamp):
	accont_holder = models.CharField(max_length = 50, null = True, blank = True)
	balance = MoneyField(
		max_digits = 19, decimal_places = 4, null = True, default_currency = 'USD', default = 0)

	def __str__(self):
		return str(self.balance)