from django.apps import AppConfig


class DjmoneyappConfig(AppConfig):
    name = 'djmoneyapp'
