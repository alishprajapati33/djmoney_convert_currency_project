from django.db import models



class TimeStamp(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(null=True, blank=True)
    is_active = models.BooleanField(default=True, null=True, blank=True)

    class Meta:
        abstract = True


class Product(TimeStamp):
	title = models.CharField(max_length = 100)
	image = models.ImageField(upload_to = "product")
	marked_price = models.DecimalField(max_digits = 20, decimal_places = 3)
	selling_price = models.DecimalField(max_digits = 20, decimal_places = 3)
	instock = models.PositiveIntegerField(default=0)

	def __str__(self):
		return self.title

