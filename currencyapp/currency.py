##Program Description:- Currency Converter
##Dev:- Joel Dcosta
##JSON API :- https://api.exchangeratesapi.io/latest?base=USD
##URL OPENSOURCE :- exchangeratesapi.io

json_url = "https://api.exchangeratesapi.io/latest?base=USD"

import urllib.request as r
import json

response = r.urlopen(json_url)
data = json.loads(response.read().decode(response.info().get_param('charset') or 'utf-8'))
##Specify what data you like to view
array = data['rates']
#print(list(array)[20])
x = list(array)
#print(x)
## 0 - 32
#print(x[26]) = USD

#n = 0
#for i in x:
       #example
       #usd = array.get(x[26])
       #i=array.get(x[n])
       #print(i+"= array.get(x["+str(n)+"])")
       #n = n + 1

CAD= array.get(x[0])
HKD= array.get(x[1])
ISK= array.get(x[2])
PHP= array.get(x[3])
DKK= array.get(x[4])
HUF= array.get(x[5])
CZK= array.get(x[6])
GBP= array.get(x[7])
RON= array.get(x[8])
SEK= array.get(x[9])
IDR= array.get(x[10])
INR= array.get(x[11])
BRL= array.get(x[12])
RUB= array.get(x[13])
HRK= array.get(x[14])
JPY= array.get(x[15])
THB= array.get(x[16])
CHF= array.get(x[17])
EUR= array.get(x[18])
MYR= array.get(x[19])
BGN= array.get(x[20])
TRY= array.get(x[21])
CNY= array.get(x[22])
NOK= array.get(x[23])
NZD= array.get(x[24])
ZAR= array.get(x[25])
USD= array.get(x[26])
MXN= array.get(x[27])
SGD= array.get(x[28])
AUD= array.get(x[29])
ILS= array.get(x[30])
KRW= array.get(x[31])
PLN= array.get(x[32])

def conv(rate, _to, _from):
       #print(1000*USD / INR)
       print(rate *  _from /  _to)