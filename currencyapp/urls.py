from django.conf import settings
from django.urls import path
from .views import *

app_name = "currencyapp"
urlpatterns = [

    # Client Panel
    path('', ClientHomeView.as_view(), name="clienthome"),
    path('currency-api/', ClientCurrencyApiView.as_view(), name="clientcurrencyapi"),


    path('select/', SelectView.as_view(), name = 'clientselect'),


    path("bank/list/", ClientBankListView.as_view(), name = 'clientbanklist'),

    path("html/svg/", ClientHtmlSvgView.as_view(), name = 'htmlsvg'),

    
]