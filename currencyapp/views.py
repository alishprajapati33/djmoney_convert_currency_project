from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth import authenticate, login, logout
from django.urls import reverse_lazy, reverse
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
# from datetime import datetime
from django.http import JsonResponse
from django.views.generic import *
from .models import *
from djmoneyapp.models import *
from .forms import *
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q

import urllib.request as r
import json
import requests


from djmoney.money import Money
from djmoney.contrib.exchange.models import convert_money
# from djmoney.contrib.exchange.backends import OpenExchangeRatesBackend
# from djmoney.contrib.exchange.models import get_rate

import djmoney_rates

class ClientMixin(object):
	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		allproducts = Product.objects.all()
		
		response = requests.get(
			"https://openexchangerates.org/api/latest.json?app_id=bea9d95fe5c84220a55787eff8438de1")
		data = response.json()
		# print(geodata)
		# print(type(geodata))
		# print(geodata["rates"])
		# print(type(geodata['rates']))

		# datarates = geodata['rates']
		# print(datarates)
		# context['HRK'] = float(datarates["NPR"])
		# print(float(datarates['NPR']))
		# context["allproducts"] = Product.objects.all()

		# response = r.urlopen("https://openexchangerates.org/api/latest.json?app_id=bea9d95fe5c84220a55787eff8438de1")
		# data = json.loads(response.read().decode(response.info().get_param('charset') or 'utf-8'))
		##Specify what data you like to view
		array = data['rates']
		# print(list(array)[50])
		x = list(array)
		# print(x)
		## 0 - 32
		#print(x[26]) = USD
		EUR= array.get(x[46])
		GBP= array.get(x[49])
		INR= array.get(x[66])
		NPR= array.get(x[108])
		USD= array.get(x[150])

		# print(type(USD))
		# ANG= array.get(x[4])
		# AOA= array.get(x[5])
		# ARS= array.get(x[6])
		# AUD= array.get(x[7])
		# AWG= array.get(x[8])
		# AZN= array.get(x[9])
		# BAM= array.get(x[10])

		# print("EUR: " + str(EUR) + " GBP: " + str(GBP) + " INR: " + str(INR) + " NPR: " + str(NPR) )

		# currency_code = self.request.GET["currency_conversion"]
		

		for n in allproducts:
			# print(n.title, n.selling_price, n.marked_price);
			# print(type(n.selling_price))
			n.selling_price = round((float(n.selling_price) * NPR / USD), 3)
			n.marked_price = round((float(n.selling_price) * NPR / USD), 3)
			# print(round(n.selling_price, 3))

		# for n in allproducts:
		# 	print('--------------------------------------')
		# 	print(n.selling_price, "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")

		context["allproducts"] = allproducts

		id = self.request.GET.get('x')
		# print(id, '*********************************')

		if id == "NPR":
			print("NPR selected?????????????????")
			
		elif id == "USD":
			print("USD selected?????????*^*^*^*^*")
		elif id == "EUR":
			print("EUr selected as well$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
		elif id == "GBP":
			print("GBP is selected ????%#%#%$#%#%#%$#%#%")
		# else:
		# 	print("something is error in this code")
		
		# def conv(rate, _USD, _NPR):
		# def conv(rate_vale, _From_currency_code, _To_cUrrency_code)
			# rate = (rate_value * _To_currency_code / _From_Currency_code)

		# rate = (1 * NPR / USD)
			# print("*******************************8")

			# return rate
		# print(rate, "******************************8")
		# print(type(datarates["HRK"]))
		# for rates in datarates:
		# 	context['rates'] = datarates

			
		return context


class ClientHomeView(ClientMixin, TemplateView):
	template_name = "clienttemplates/clienthome.html"

	def POST(request):
		currency = request.POST.get(data)
		print(currency, 'aksjdhflasdhflasdhflksdh')



class ClientCurrencyApiView(ClientMixin, TemplateView):
	template_name = "clienttemplates/clientcurrencyapi.html"

class SelectView(View):
    def get(self, request, **kwargs):
        return render(request, "clienttemplates/clientselect.html", {})
    
    def post(self, request, *args, **kwargs):
        led_sw = request.POST.get('selectname')
        if led_sw == "a":
            print("USD is selected")
        elif led_sw == 'b':
            print("NPR is selected")
        elif led_sw == 'c':
        	print("UER is selecename")
        else:
        	print("GBP is sellcetd")
        
        return render(request, "clienttemplates/clientselect.html", {})



class ClientBankListView(ClientMixin, TemplateView):
	template_name = "clienttemplates/clientbanklist.html"

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context["accounts"] = BankAccount.objects.all()

		account = BankAccount.objects.all()
		
		for n in account:
			# print(n.accont_holder);
			# print(n.balance);
			# a = convert_money(Money(n.balance, 'n.currency') 'USD')
			print(n.balance.currency)
			# value = Money(n.balance.amount, 'EUR')
			# a = convert_money(n.balance.amount, "n.balance.currency", "USD")
			a = convert_money(n.balance, "USD")
			# print(a)
			n.balance = a
			print(n.balance)
		return context



class ClientHtmlSvgView(ClientMixin, TemplateView):
	template_name = "clienttemplates/clienthtmlsvg.html"

